window.onload = function(){
	function Add(num) {
		var elem1 = document.getElementById('final');
		return Undoit.Action().init(function(){
			elem1.innerText = Number(elem1.innerText)+num;
		}).exec(function(){
			var def = this;
			setTimeout(function(){
				var elem2 = document.getElementById('deferred');
				elem2.innerText = Number(elem2.innerText)+num;
				def.done();
			},800);
		});
	}
	
	document.getElementById('test').onclick = function(){
		var rand = Math.floor(Math.random() * 6) + 1;
		Undoit.register({
			redo	 : Add(rand),
			//redoArgs : [rand],
			undo	 : Add(-rand),
			//undoArgs : [-rand]
		});
	};
	
	document.getElementById('undo').onclick = function(){
		Undoit.undo();
	};
	
	document.getElementById('redo').onclick = function(){
		Undoit.redo();
	};
	
	document.getElementById('clear').onclick = function(){
		Undoit.clear();
	};	
};
