var Undoit = (function(){
	var _queue = [];
	var _undo  = [];
	var _redo  = [];
	var _locked = false;
	
	function processQueue() {
		if (!_locked && _queue.length) {	//Not locked && queue not empty
			_locked = true;					//Lock thread
			_queue[0]						//Get item from queue, and...
			.done(function(num) {			//...when complete:
				_queue.shift();					//Shift action off queue
				_locked = false;				//Unlock thread
				processQueue();					//Continue thread
			})
			.exec();						//Execute action
		}
	}
	
	return {
		register: function(item) {
			_undo.push(item);				//Add item to undo stack
			_redo = [];						//Reset the redo stack
			_queue.push(					//Queue redo action
				item.redo.init()			//
			);
			processQueue();					//Start process queue
		},
		undo: function() {
			if (_undo.length > 0) {			//Undo stack is not empty
				var item = _undo.pop();		//Pop undo item off stack
				_redo.push(item);			//Add item to redo stack
				_queue.push(				//Queue undo action
					item.undo.init()
				);
				processQueue();				//Start process queue
			}
		},
		redo: function() {
			if (_redo.length > 0) {			//Redo stack is not empty
				var item = _redo.pop();		//Pop redo item off stack
				_undo.push(item);			//Add item to undo stack
				_queue.push(				//Queue redo action
					item.redo.init()
				);
				processQueue();				//Start process queue
			}
		},
		clear: function() {
			_undo = [];
			_redo = [];
		},
		isLocked: function() {
			return _locked;
		},
		Action: function() {
			var _init = [];
			var _exec = [];
			var _done = [];
			
			return {
				init : function(func) {
					if(typeof(func) == 'function') {
						_init.push(func);
					} else {
						for(var a=0; a<_init.length;a++) {
							_init[a].apply(this);
						}
					}
					return this;
				},
				exec : function(func) {
					if(typeof(func) == 'function') {
						_exec.push(func);
					} else {
						for(var a=0;a<_exec.length;a++) {
							_exec[a].apply(this);
						}
					}
					return this;
				},
				done : function(func) {
					if(typeof(func) == 'function') {
						_done.push(func);
					} else {
						for(var a=0;a<_done.length;a++) {
							_done[a].apply(this);
						}
					}
					return this;
				}
			};
		}
	};
})();